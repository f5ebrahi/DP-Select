import multiprocessing
import pandas as pd
import numpy as np
import torch
import argparse
import json
import os
import re


def find_range(df1, df2):
    range_ = []
    for attr in df1.columns:
        range_.append([min(min(df1[attr]), min(df2[attr]))-5, max(max(df1[attr]), max(df2[attr]))+5])
    return range_

def encode_data(dataframe, config):
    
	#Faeze:
	out = pd.DataFrame()
	mapping_dicts = {}
	one_hot_columns = {}

	comp = pd.read_csv(os.path.dirname(__file__) + '/../../'+config["complementary"])
	#dataframe.append(comp)
	dataframe = pd.concat([dataframe, comp])

	for col_index, col_name in enumerate(dataframe.columns):
		if col_name == config["label"]:
			if config["dataset_name"] == "adult":	
				out = pd.concat([out, pd.get_dummies(dataframe[col_name], prefix=col_name)], axis=1)
				out = out.drop(["income_<=50K"], axis=1)
				out = out.rename(columns={"income_>50K": "income"})
				mapping_dicts[col_name] = {0: "<=50K", 1: ">50K"}
		elif col_index in config["one-hot_cols"]:
			if bool(config['numerize']):
				dataframe[col_name] = dataframe[col_name].astype('category')
				out = pd.concat([out, dataframe[col_name].cat.codes], axis=1)
				out.columns = [*out.columns[:-1], col_name]
				mapping_dicts[col_name] = dict(enumerate(dataframe[col_name].cat.categories))
			else:
				start = len(out.columns)
				out = pd.concat([out, pd.get_dummies(dataframe[col_name], prefix=col_name)], axis=1)
				end = len(out.columns)
				one_hot_columns[col_name] = [start, end]
		elif col_index in config["gmm_cols"]:
			out = pd.concat([out, dataframe[col_name]], axis=1)
		else:
			print("the column type is not recognized")
			continue
	#Faeze_end
	out.drop(out.tail(len(comp)).index, inplace = True)
	return out, mapping_dicts, one_hot_columns

def decode_data(dataframe, mapping_dicts, one_hot_columns, col_number, config):
    
	out = pd.DataFrame()
	dataframe.update(dataframe[list(mapping_dicts)].apply(lambda col: col.map(mapping_dicts[col.name])))
	if config['numerize']:
		out = dataframe
	else:
		out = pd.DataFrame()
		size_ = 0
		for col_name, one_hot in one_hot_columns.items():
			print(one_hot)
			print(col_name)
			if col_name == config["label"]:
				continue
			elif one_hot[0] > size_:
				out = pd.concat([out, dataframe.iloc[:, size_:one_hot[0]]], axis=1)
				out = pd.concat([out, dataframe.iloc[:, one_hot[0]:one_hot[1]].astype(np.float).idxmax(axis=1)], axis=1)
				out.columns = [*out.columns[:-1], col_name]
				sub_string = col_name+"_"
				out[col_name] = out[col_name].str.replace(sub_string,"")
				size_ = one_hot[1]
			else:
				out = pd.concat([out, dataframe.iloc[:, one_hot[0]:one_hot[1]].astype(np.float).idxmax(axis=1)], axis=1)
				out.columns = [*out.columns[:-1], col_name]
				sub_string = col_name+"_"
				out[col_name] = out[col_name].str.replace(sub_string,"")
				size_ = size_+one_hot[1]-one_hot[0]
		print(col_number)
		if len(out.columns)<col_number:
			out = pd.concat([out, dataframe.iloc[:, size_:]], axis=1)
	return out
