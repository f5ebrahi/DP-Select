import numpy as np
import pandas as pd
from numba import jit, cuda


def sum2D(input):
    return sum(map(sum, input))



def calc_MI(X,Y,bins):

   c_XY = np.histogram2d(X,Y,bins)[0]
   c_X = np.histogram(X,bins)[0]
   c_Y = np.histogram(Y,bins)[0]

   H_X = shan_entropy(c_X)
   H_Y = shan_entropy(c_Y)
   H_XY = shan_entropy(c_XY)

   MI = H_X + H_Y - H_XY
   return MI

def shan_entropy(c):
    c_normalized = c / float(np.sum(c))
    c_normalized = c_normalized[np.nonzero(c_normalized)]
    H = -sum(c_normalized* np.log2(c_normalized))  
    return H



def best_marginal(df, train_data_range, bins, num_marginals):
    n = len(df.columns)
    matMI = np.zeros((n, n))
    for ix in np.arange(n):
        for jx in np.arange(ix+1,n):
            matMI[ix,jx] = calc_MI(df.iloc[:,ix].values, df.iloc[:,jx].values, bins)

    best_marginals = np.dstack(np.unravel_index(np.argsort(-matMI.ravel()), (n, n)))[0][:num_marginals]
    
    best_marginal_weights = []
    best_marginal_histograms = []
    for marginal in best_marginals:
        temp_hist = np.histogram2d(df.iloc[:,marginal[0]].values,df.iloc[:,marginal[1]].values,
                                                       bins,range=[train_data_range[marginal[0]], train_data_range[marginal[1]]])[0]
        if sum2D(temp_hist)>0:
            temp_hist = temp_hist/sum2D(temp_hist)   #automatically normalizes?
        best_marginal_histograms.append(temp_hist)
        best_marginal_weights.append(matMI[marginal[0], marginal[1]])
#    print(best_marginal_weights)
#    print(best_marginals)
    best_marginal_weights = [float(i)/sum(best_marginal_weights) for i in best_marginal_weights]
    
    #print(best_marginal_histograms)
    return best_marginals, best_marginal_histograms, best_marginal_weights
