import pandas as pd
import numpy as np
from numba import jit, cuda
import random
from multiprocessing import Process, Value, Array
import multiprocessing as mp


def sum2D(input):
    return sum(map(sum, input))

def compute_score_(distance_measure, temp, train_data_range, bins, best_marginal, best_marginal_histograms, i, best_marginal_weights):
    temp_hist = np.histogram2d(temp.iloc[:,best_marginal[0]].values,temp.iloc[:,best_marginal[1]].values,
                                                    bins,range=[train_data_range[best_marginal[0]], 
                                                                        train_data_range[best_marginal[1]]])[0]
    if sum2D(temp_hist)>0:
        temp_hist = temp_hist/sum2D(temp_hist)     # I think it automatically normalizes I don't need to normalize again
    if distance_measure == 'Density_distance_area':
        score = best_marginal_weights[i]* sum2D(np.absolute(best_marginal_histograms[i] - temp_hist))
    elif distance_measure == 'Kolmogorov_distance':
        score = best_marginal_weights[i]* max(map(max, (np.absolute(best_marginal_histograms[i] - temp_hist))))
    elif distance_measure == 'l2':
        score = best_marginal_weights[i]* sum2D((np.absolute(best_marginal_histograms[i] - temp_hist))**2)
    
    return score


def compute_score(distance_measure, out, random_sample, train_data_range, bins, best_marginals, best_marginal_histograms, best_marginal_weights):
    
    score = 0
    temp = pd.concat([out, random_sample], ignore_index = True, axis = 0)
    for i, best_marginal in enumerate(best_marginals):
        score = score + compute_score_(distance_measure, temp, train_data_range, bins, best_marginal, best_marginal_histograms, i, best_marginal_weights)
    return -score

'''''
'@jit(target_backend='cuda')
def compute_score_(args, q: mp.Queue):
    i = args['i']
    temp_hist = np.histogram2d(args['temp'].iloc[:,args['best_marginals'][i][0]].values,args['temp'].iloc[:,args['best_marginals'][i][1]].values,
                                                    args['bins'],range=[args['train_data_range'][args['best_marginals'][i][0]], 
                                                                        args['train_data_range'][args['best_marginals'][i][1]]])[0]
    if sum2D(temp_hist)>0:
        temp_hist = temp_hist/sum2D(temp_hist)     # I think it automatically normalizes I don't need to normalize again
    if args['distance_measure'] == 'Density_distance_area':
        score = args['best_marginal_weights'][i]* sum2D(np.absolute(args['best_marginal_histograms'][i] - temp_hist))
    elif args['distance_measure'] == 'Kolmogorov_distance':
        score = args['best_marginal_weights'][i]* max(map(max, (np.absolute(args['best_marginal_histograms'][i] - temp_hist))))
    elif args['distance_measure'] == 'l2':
        score = args['best_marginal_weights'][i]* sum2D((np.absolute(args['best_marginal_histograms'][i] - temp_hist))**2)
    
    q.put(score)
    return score

def compute_score(distance_measure, out, random_sample, train_data_range, bins, best_marginals, best_marginal_histograms, best_marginal_weights):
    
    score = 0
    temp = pd.concat([out, random_sample], ignore_index = True, axis = 0)
    for i in enumerate(best_marginals):
        score = score + compute_score_(distance_measure, temp, train_data_range, bins, best_marginals, best_marginal_histograms, i, best_marginal_weights)
    score = -score
    
    num = Value('d', 0.0)
    i = Array('i', 0.0)

    p = Process(target=compute_score_, args=(num, arr))
    p.start()
    p.join()
    
    print (best_marginals)
    arg_list = []
    for i in enumerate(best_marginals):
        arg_list.append({'i': i,
                         'distance_measure': distance_measure, 
                         'temp': temp, 
                         'train_data_range': train_data_range, 
                         'bins': bins, 
                         'best_marginals': best_marginals, 
                         'best_marginal_histograms': best_marginal_histograms, 
                         'best_marginal_weights': best_marginal_weights})
    
    manager = mp.Manager()
    q = manager.Queue()
    pool = mp.Pool(4) #num_workers = 4?

    jobs = []
    for arg in arg_list:
        job = pool.apply_async(compute_score_, (arg, q))
        jobs.append(job)

    for job in jobs:
        job.get()

    print(q)
    q.put('kill')
    pool.close()
    pool.join()'''
    
    

def compute_BR(epsilon_s, delta, N):
    inner_term = epsilon_s / (-np.expm1(-epsilon_s))
    first_term = N * (inner_term - 1 - np.log(inner_term))
    epsilon = first_term + np.sqrt(N * 0.5 * (epsilon_s ** 2) * np.log(1 / delta))
    return epsilon


def bounded_range_composition(epsilon, delta, N, epsilon_step_limit):
    epsilon_s = epsilon/N
    epsilon_step = epsilon_s
    epsilon_BR = compute_BR(epsilon_s, delta, N)
    if epsilon_BR > epsilon:
        return epsilon_s
    while epsilon_step > epsilon_step_limit:
        epsilon_s = epsilon_s + epsilon_step
        epsilon_BR = compute_BR(epsilon, delta, N)
        if epsilon_BR > epsilon:
            epsilon_s = epsilon_s - epsilon_step
            epsilon_step = epsilon_step/2
    return epsilon_s


def exponential_mechanism(scores, sensitivity, epsilon): 
    # Calculate the probability for each element, based on its score
    probabilities = [np.exp(epsilon * score / (2 * sensitivity)) for score in scores]
    # Normalize the probabilties so they sum to 1
    #if np.linalg.norm(probabilities, ord=1) != 0:
    #    probabilities = probabilities / np.linalg.norm(probabilities, ord=1)
    # Choose an element from R based on the probabilities
    out = random.choices(range(len(scores)), weights=probabilities, k=1)[0]
    return out


def select_best(out_marginals, random_samples, train_data_range, bins, best_marginals, best_marginal_histograms, best_marginal_weights, distance_measure, sensitivity, noise):
    score = []
    for i in range(len(random_samples)):
        score.append(compute_score(distance_measure, out_marginals, random_samples.iloc[[i]], train_data_range, bins, best_marginals, best_marginal_histograms, best_marginal_weights))
    
    #add noise
    private_score = exponential_mechanism(score, sensitivity, noise)
    
    return random_samples.iloc[[private_score]]

def random_pick_samples(data, out, k):
    new = data.sample(k, replace=True)
    common_samples = out.merge(new, how = 'inner' ,indicator=False)
    while len(common_samples)>0:
        print("repeated sample")
        new = data.sample(k, replace=True)
        #new = pd.concat([out, common_samples]).drop_duplicates(keep=False)
        #out = pd.concat([data.sample(len(common_samples)), out]
        common_samples = out.merge(new, how = 'inner' ,indicator=False)
    return new


def selection(best_marginals, best_marginal_histograms, best_marginal_weights, distance_measure, generated_data, N, train_data_range, bins, k, noise):
    
    # compute epsilon from composition theorem
    delta = 1e-5
    epsilon_step_limit = 1e-4
    epsilon = bounded_range_composition(noise, delta, N, epsilon_step_limit)
    
    out = pd.DataFrame(columns = generated_data.columns)
    for i in range(N):
        random_samples = random_pick_samples(generated_data, out, k)
        new_sample = select_best(out, random_samples, 
                                 train_data_range, bins,
                                 best_marginals, best_marginal_histograms, 
                                 best_marginal_weights, distance_measure,
                                 1/N, epsilon)
        generated_data = pd.concat([generated_data, new_sample]).drop_duplicates(keep=False)
        out = pd.concat([out, new_sample], ignore_index = True, axis = 0)
        if i%500==0:
            print("i = "+str(i))
            print(out)
    
    return out