'''
These functions are the composition theorems used for differential privacy
'''

import numpy as np

def none_private(number_selections, epsilon, delta):
    return 1

def naive_comp(number_selections, epsilon, delta):
    eps_per_sample = epsilon / number_selections
    return eps_per_sample

def bounded_range_comp(number_selections, epsilon, delta):
    eps_per_sample = bisection_search_epsilon(number_selections, epsilon, delta, dong_comp)
    return eps_per_sample
