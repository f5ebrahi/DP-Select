import pandas as pd
import numpy as np
from numba import jit, cuda
import random
from multiprocessing import Process, Value, Array
import multiprocessing as mp
import statistics


def sum2D(input):
    return sum(map(sum, input))

def compute_score_(distance_measure, temp, train_data_range, bins, best_marginal, best_marginal_histograms, i, best_marginal_weights):
    temp_hist = np.histogram2d(temp.iloc[:,best_marginal[0]].values,temp.iloc[:,best_marginal[1]].values,
                                                    bins,range=[train_data_range[best_marginal[0]], 
                                                                        train_data_range[best_marginal[1]]])[0]
    if sum2D(temp_hist)>0:
        temp_hist = temp_hist/sum2D(temp_hist)     # I think it automatically normalizes I don't need to normalize again
    if distance_measure == 'Density_distance_area':
        #score = best_marginal_weights[i]* sum2D(np.absolute(best_marginal_histograms[i] - temp_hist))
        score = sum2D(np.absolute(best_marginal_histograms[i] - temp_hist))

    elif distance_measure == 'Kolmogorov_distance':
        #score = best_marginal_weights[i]* max(map(max, (np.absolute(best_marginal_histograms[i] - temp_hist))))
        score = max(map(max, (np.absolute(best_marginal_histograms[i] - temp_hist))))
    
    elif distance_measure == 'l2':
        #score = best_marginal_weights[i]* sum2D((np.absolute(best_marginal_histograms[i] - temp_hist))**2)
        score = sum2D((np.absolute(best_marginal_histograms[i] - temp_hist))**2)

    
    return score


def compute_score(distance_measure, temp, train_data_range, bins, best_marginals, best_marginal_histograms, best_marginal_weights):
    
    score = []
    for i, best_marginal in enumerate(best_marginals):
        score.append(compute_score_(distance_measure, temp, train_data_range, bins, best_marginal, best_marginal_histograms, i, best_marginal_weights))
    return score


import configparser
import pandas as pd
import numpy as np
import torch
import argparse
import json
import os
import pathlib

from dataset_process import encode_data, decode_data, find_range
from best_marginal import best_marginal


def run_marginal_eval(config):
    
	### load and pre-process the needed datasets:
    train = pd.read_csv(str(pathlib.Path(__file__).parent.resolve()) + '/../'+config["train"])
    col_num = len(train.columns)
    train_, mapping_dicts, one_hot_columns = encode_data(train, config)
    name = config["GAN_output"]
    idx = len(name)-5
    for j in range(config["pool_size"]):
        if j == 0:
            GAN_output = pd.read_csv(str(pathlib.Path(__file__).parent.resolve()) + '/../'+name)
        else:
        	GAN_output = pd.concat([GAN_output, pd.read_csv(str(pathlib.Path(__file__).parent.resolve()) + '/../'+ name[:idx] + str(j) + name[idx+1:])])
	#GAN_output = pd.read_csv(os.path.dirname(__file__) + '/../'+config["GAN_output"])
    GAN_output.drop_duplicates(inplace=True)
    GAN_output.reset_index(drop=True, inplace=True)
    GAN_output_, mapping_dicts, one_hot_columns = encode_data(GAN_output, config)
    data_range = find_range(GAN_output_,train_)
    
    if config["first_time"]:
        best_marginals_, best_marginal_histograms_, best_marginal_weights_ = best_marginal(train_, data_range, 
																				config["bins"], 1*config["num_marginals"])
        best_marginals = random.choices(best_marginals_, k=config["num_marginals"])
        best_marginal_histograms = random.choices(best_marginal_histograms_, k=config["num_marginals"])
        best_marginal_weights = random.choices(best_marginal_weights_, k=config["num_marginals"])
        pd.DataFrame(best_marginals).to_csv("best_marginals.csv", index=False)
        pd.DataFrame(np.dstack(best_marginal_histograms).flatten()).to_csv("best_marginal_histograms.csv", index=False)
        pd.DataFrame(best_marginal_weights).to_csv("best_marginal_weights.csv", index=False)
    else:
        best_marginals=pd.read_csv("best_marginals.csv").to_numpy()
        best_marginal_histograms=list(pd.read_csv("best_marginal_histograms.csv").to_numpy().reshape((config["num_marginals"], config["bins"], config["bins"])))
        best_marginal_weights=pd.read_csv("best_marginal_weights.csv").to_numpy()
        
  
    
    scores_GAN = compute_score(config['distance_measure'], GAN_output_, data_range, config["bins"], best_marginals, best_marginal_histograms, best_marginal_weights)

    path = "expdir/final_results/"+config["name"]
    generated_dataset = pd.read_csv(path)
    generated_dataset_, mapping_dicts, one_hot_columns = encode_data(generated_dataset, config)
    data_range = find_range(generated_dataset_,train_)
    scores_generated = compute_score(config['distance_measure'], generated_dataset_, data_range, config["bins"], best_marginals, best_marginal_histograms, best_marginal_weights)

    subtract_scores = [x1 - x2 for (x1, x2) in zip(scores_GAN, scores_generated)]
    max_score = max(subtract_scores)
    mean_score = sum(subtract_scores)/len(subtract_scores)
    #median_score = statistics.median(subtract_scores)
    mean_GAN = sum(scores_GAN)/len(scores_GAN)
    mean_generated = sum(scores_generated)/len(scores_generated)
    
    path = "expdir/final_results/"+"results_marginal_eval.txt"
    with open(path, 'a') as f:
        f.write('max GAN-generated score: %s \n' % (str(max_score)))
        f.write('mean GAN-generated score: %s \n' % (str(mean_score)))
        #f.write('median GAN-generated score: %s \n' % (str(median_score)))
        f.write('GAN difference score mean: %s \n' % (str(mean_GAN)))
        f.write('generated difference score mean: %s \n' % (str(mean_generated)))
    return 

if __name__ == "__main__":
	#os.environ["CUDA_VISIBLE_DEVICES"] = "2"
	parser = argparse.ArgumentParser()
	parser.add_argument('configs', help='a json config file')
	args = parser.parse_args()
	with open(args.configs) as f:
		configs = json.load(f)
		
	try:
		os.mkdir("expdir")
	except:
#import dataset_process
		pass
		
	for config in configs:
		path = "expdir/final_results/"+"results_marginal_eval.txt"
		with open(path, 'a') as f:
		    f.write('*** NAME: %s *** \n' % (config["name"]))
		run_marginal_eval(config)
			

	
