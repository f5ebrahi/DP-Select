# spot check machine learning algorithms on the adult imbalanced dataset
from numpy import mean
from numpy import std
import pandas as pd
from pandas import read_csv
from matplotlib import pyplot
from sklearn.preprocessing import LabelEncoder
from sklearn.preprocessing import OneHotEncoder
from sklearn.preprocessing import MinMaxScaler
from sklearn.pipeline import Pipeline
from sklearn.compose import ColumnTransformer
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import RepeatedStratifiedKFold
from sklearn.tree import DecisionTreeClassifier
from sklearn.svm import SVC
from sklearn.ensemble import RandomForestClassifier
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.ensemble import BaggingClassifier
from sklearn.metrics import f1_score, accuracy_score, fbeta_score, roc_auc_score
from sklearn.preprocessing import MinMaxScaler

def train_predict(learner, sample_size, X_train, y_train, X_test, y_test):
    results = {}
    learner = learner.fit(X_train, y_train)
    predictions_test = learner.predict(X_test)
    
    results['acc_test'] = accuracy_score(y_test, predictions_test)
    results['f_test'] = fbeta_score(y_test, predictions_test, beta=0.5)
    print("acc_test,        f_test ")
    print(results)
    return results

# load the dataset
def load_dataset(full_path):
	# load the dataset as a numpy array
    data = read_csv(full_path, header=None, na_values='?')
    # prep:
    data = data[data["workclass"] != "?"]
    data = data[data["occupation"] != "?"]
    data = data[data["native-country"] != "?"]
	# drop rows with missing
	# dataframe = dataframe.dropna()
	# split into inputs and outputs
	# last_ix = len(dataframe.columns) - 1
	#data, y = dataframe.drop(last_ix, axis=1), dataframe[last_ix]
    # Normalization
    scaler = MinMaxScaler()
    numerical = ['age','educational-num', 'gain', 'loss', 'hours']
    features_log_minmax_transform = pd.DataFrame(data = features_log_transformed)
    features_log_minmax_transform[numerical] = scaler.fit_transform(features_log_transformed[numerical])
    
    #Preprocessing categorical features
    features_final = pd.get_dummies(features_log_minmax_transform)
    income = income_raw.map({'<=50K':0, '>50K':1})
    encoded = list(features_final.columns)
    
    # feature selection
    col_names = data.columns
    param=[]
    correlation=[]
    abs_corr=[]
    for c in col_names:
        if c!="income":
            if len(data[c].unique()) <= 2:
                corr = spearmanr(data['income'],data[c])[0]
            else:
                corr = pontbiserialr(data['income'],data[c])[0]
            param.append(c)
            correlation.append(corr)
            abs_corr.append(abs(corr))
    param_df = pd.DataFrame({'correlation':correlation, 'parameter':param, 'abs_corr':abs_corr})
    param_df = param_df.sort_values(by=['abs_corr'], ascending=False)
    param_df = param_df.set_index('parameter')
    
    random_state = 42
    clf_A = RandomForestClassifier(random_state=random_state)
    clf_B = GaussianNB()
    
    results = {}
    for clf in [clf_A, clf_B]:
        clf_name = clf.__class__.__name__
        results[clf_name] = {}
        results[clf_name][i] = train_predict(clf, X_train, y_train, X_test, y_test)
    
	# select categorical and numerical features
    cat_ix = X.select_dtypes(include=['object', 'bool']).columns
    num_ix = X.select_dtypes(include=['int64', 'float64']).columns
	# label encode the target variable to have the classes 0 and 1
    y = LabelEncoder().fit_transform(y)
    return X.values, y, cat_ix, num_ix

# evaluate a model
def evaluate_model(X, y, X_test, y_test, model):
    model.fit(X, y)
    predictions = model.predict(X_test)
    scores = []
                
    scores.append(f1_score(y_test[1:], predictions[1:]))
    scores.append(accuracy_score(y_test[1:], predictions[1:]))
    scores.append(roc_auc_score(y_test[1:], predictions[1:]))
    return scores

# define models to test
def get_models():
	models, names = list(), list()
	# CART
	models.append(DecisionTreeClassifier())
	names.append('CART')
	# SVM
	models.append(SVC(gamma='scale'))
	names.append('SVM')
	# Bagging
	models.append(BaggingClassifier(n_estimators=100))
	names.append('BAG')
	# RF
	models.append(RandomForestClassifier(n_estimators=100))
	names.append('RF')
	# GBM
	models.append(GradientBoostingClassifier(n_estimators=100))
	names.append('GBM')
	return models, names

def eval(full_path_train, full_path_generated, full_path_test, full_path_GAN, result_path):
    # load the dataset
    X_train, y_train, cat_ix, num_ix = load_dataset(full_path_train)
    X_generated, y_generated, cat_ix, num_ix = load_dataset(full_path_generated)
    X_GAN, y_GAN, cat_ix, num_ix = load_dataset(full_path_GAN)
    X_test, y_test, cat_ix, num_ix = load_dataset(full_path_test)
    # define models
    models, names = get_models()
    results_train = list()
    results_generated = list()
    results_GAN = list()
    
    # evaluate each model
    for i in range(len(models)):
        # define steps
        steps = [('c',OneHotEncoder(handle_unknown='ignore'),cat_ix), ('n',MinMaxScaler(),num_ix)]
        # one hot encode categorical, normalize numerical
        ct = ColumnTransformer(steps)
        # wrap the model i a pipeline
        pipeline = Pipeline(steps=[('t',ct),('m',models[i])])
        # evaluate the model and store results
        scores_train = evaluate_model(X_train, y_train, X_test, y_test, pipeline)
        results_train.append(scores_train)
        # summarize performance
        print('> train: %s f1_score %.3f  accuracy %.3f roc_auc %.3f' % (names[i], scores_train[0], scores_train[1], scores_train[2]))
        with open(result_path, 'a') as f:
            print('> train: %s f1_score %.3f  accuracy %.3f roc_auc %.3f' % (names[i], scores_train[0], scores_train[1], scores_train[2]), file=f)

        # same for GAN data:
        scores_GAN = evaluate_model(X_GAN, y_GAN, X_test, y_test, pipeline)
        results_GAN.append(scores_GAN)
        # summarize performance
        print('> GAN: %s f1_score %.3f  accuracy %.3f roc_auc %.3f' % (names[i], scores_GAN[0], scores_GAN[1], scores_GAN[2]))
        with open(result_path, 'a') as f:
            print('> GAN: %s f1_score %.3f  accuracy %.3f roc_auc %.3f' % (names[i], scores_GAN[0], scores_GAN[1], scores_GAN[2]), file=f)

        # same for generated data:
        scores_generated = evaluate_model(X_generated, y_generated, X_test, y_test, pipeline)
        results_generated.append(scores_generated)
        # summarize performance
        print('> generated: %s f1_score %.3f  accuracy %.3f roc_auc %.3f' % (names[i], scores_generated[0], scores_generated[1], scores_generated[2]))
        with open(result_path, 'a') as f:
            print('> generated: %s f1_score %.3f  accuracy %.3f roc_auc %.3f' % (names[i], scores_generated[0], scores_generated[1], scores_generated[2]), file=f)