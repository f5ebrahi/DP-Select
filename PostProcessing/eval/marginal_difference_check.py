import configparser
import multiprocessing
import pandas as pd
import numpy as np
import torch
import argparse
import json
import os
import pathlib
from numba import jit, cuda
import csv

from dataset_process import encode_data, decode_data, find_range
from best_marginal import best_marginal
from selection import selection
from eval import eval

import time

def compare(s, t):
    t = list(t)   # make a mutable copy
    s = list(s)
    print(s)
    print(t)
    try:
        print("no valueerror")
        for elem in s:
            print(elem)
            t.remove(elem)
            print("removed"+str(elem))
    except ValueError:
        return False
    print(not t.any())
    return not t.any()

def run(config):
    ### load and pre-process the needed datasets:
	train = pd.read_csv(str(pathlib.Path(__file__).parent.resolve()) + '/../'+config["train"])
	col_num = len(train.columns)
	train_, mapping_dicts, one_hot_columns = encode_data(train, config)
	name = config["GAN_output"]
	idx = len(name)-5
	for j in range(config["pool_size"]):
		if j == 0:
			GAN_output = pd.read_csv(str(pathlib.Path(__file__).parent.resolve()) + '/../'+name)
		else:
			GAN_output = pd.concat([GAN_output, pd.read_csv(str(pathlib.Path(__file__).parent.resolve()) + '/../'+ name[:idx] + str(j) + name[idx+1:])])
	#GAN_output = pd.read_csv(os.path.dirname(__file__) + '/../'+config["GAN_output"])
	GAN_output.drop_duplicates(inplace=True)
	GAN_output.reset_index(drop=True, inplace=True)
	GAN_output_, mapping_dicts, one_hot_columns = encode_data(GAN_output, config)
	data_range = find_range(GAN_output_,train_)
	###

	train_shuffled = train_.sample(frac=1)
	train_list = np.array_split(train_shuffled, config['section_num'])
	GAN_out_shuffled = GAN_output_.sample(frac=1)
	GAN_out_list = np.array_split(GAN_out_shuffled, config['section_num'])
	start_time = time.time()
	diff_counts=[]
	with open('diff_counts.csv', mode='w') as f:
		writer = csv.writer(f, delimiter=',')

		writer.writerow(['# removed records', 'different marginals'])

		for j in range(200):
			for i in range(config['section_num']):
				test = train_shuffled.sample(frac=(len(train)-1-12-j)/len(train))
				best_marginals, best_marginal_histograms, best_marginal_weights = best_marginal(test, data_range, config["bins"], config["num_marginals"])
				ms = best_marginals
				if i==0:
					ms0=ms
					test0=test
					diff_count=0
				if test.equals(test0):
					print("SAME SAMPLE")
				if (ms[:, None] == ms0).all(axis=2).any(axis=0).all():
					continue
				else:
					print("different")
					diff_count = diff_count+1
  		#for i in best_marginals:
		#	print("("+train.columns[i[0]]+","+train.columns[i[1]]+")")
#		if i==0:
#			generated_dataset_ = selection(best_marginals, best_marginal_histograms, best_marginal_weights, config['distance_measure'], GAN_out_list[i], 
#									int(config["target_dataset_size"]/config['section_num']), data_range, config["bins"], config["selection_range"], config["selection_noise"]/config['section_num'])
#		else:
#			generated_dataset_ = pd.concat([generated_dataset_, selection(best_marginals, best_marginal_histograms, 
#                                                                best_marginal_weights, config['distance_measure'], GAN_out_list[i], 
#																int(config["target_dataset_size"]/config['section_num']), data_range, 
#                												config["bins"], config["selection_range"], config["selection_noise"]/config['section_num'])])
			print("marginal selection diff count for "+str(j+12+1)+" removed points:")
			print(diff_count)
			writer.writerow([str(j+12+1), str(diff_count)])
			diff_counts.append(diff_count)

	#file = open('diff_counts.csv', 'w+', newline ='')
 
	# writing the data into the file
	#with file:   
	#	write = csv.writer(file)
	#	write.writerows(diff_counts)
  
	print("TIME: "+str(time.time()-start_time)+" seconds")

#	generated_dataset = decode_data(generated_dataset_, mapping_dicts, one_hot_columns, col_num, config)
	#print(generated_dataset)
#	path = "expdir/final_results/"+config["name"]
#	generated_dataset.to_csv(path, index=False)
	return

if __name__ == "__main__":
	#os.environ["CUDA_VISIBLE_DEVICES"] = "2"
	parser = argparse.ArgumentParser()
	parser.add_argument('configs', help='a json config file')
	args = parser.parse_args()
	with open(args.configs) as f:
		configs = json.load(f)
		
	try:
		os.mkdir("expdir")
	except:
#import dataset_process
		pass
		
	for config in configs:
		if config["just_eval"]:
			path = "expdir/final_results/"+"results_eval.txt"
			with open(path, 'a') as f:
				f.write('*** NAME: %s *** \n' % (config["name"]))
			eval(os.path.dirname(__file__) + '/../'+config["train"], 
        			'expdir/final_results/'+config["name"], 
        			os.path.dirname(__file__) + '/../'+config["sample"], 
        			config["GAN_output"], 
        			path,
           			config["pool_size"])
		else:
			

			run(config)
	
	'''		jobs = [multiprocessing.Process(target=thread_run, args=(path, config))]	#need to rewrite this
			for j in jobs:
				j.start()
			for j in jobs:
				j.join()
'''
